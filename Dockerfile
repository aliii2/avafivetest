# Stage 1: Build
FROM node:14 AS build

WORKDIR /usr/src/app

COPY package.json .
COPY package-lock.json .

# Install dependencies for production
RUN npm ci --production

# Stage 2: Production image
FROM node:14-alpine

WORKDIR /usr/src/app

COPY --from=build /usr/src/app/node_modules ./node_modules
COPY . .

EXPOSE 3000

CMD ["npm", "start"]









# FROM node:14 


# WORKDIR  /usr/src/app

# COPY package.json .

# RUN npm install

# RUN npm install -g nodemon


# COPY . .

# EXPOSE 3000

# CMD ["sh", "-c", "npm test && nodemon --watch ."]

